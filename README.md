![reykjavik-rocks.jpg](http://i.imgur.com/tgscccL.jpg)
### Complete code of **[Reykjavik Rocks](http://reykjavik.rocks)** website - best luxury agent in Iceland. ###
Reykjavik Rocks is a high end, one-stop, service company for those who like to have a great time while in Iceland.

### UPDATES ###
* **30/4/15**
Content scaling for iPhone4/5 fixed a bit more + 'x' icon made bigger. Mobile detection added to disable background video on mobile (because of autoplay not working).

* **29/4/15**
Content scaling for iPhone4/5 & smaller phones fixed + missing z-index for 'x' icon added.

* **13/4/15**
All ready.
Few issues fixed + responsive styles added.

* **11/4/15**
99% ready - missing responsive styles
...but looks quite nice on desktop. ;]

### LINKS ###
* [http://reykjavik.rocks](http://reykjavik.rocks)
* [https://facebook.com/reykjavikrocks.is](https://facebook.com/reykjavikrocks.is)